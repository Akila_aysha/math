// app.module.ts 
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { NgModel } from '@angular/forms';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
// application
import {AppComponent} from "./app.component";
import {CommentService} from "./api.service";
import { ThreeMatrixComponent } from "./MatrixProblems/3_3/3_3.component";
import { Routes, RouterModule } from '@angular/router';
import { NewtonRaphsonComponent } from "./newtonRaphson/newtonraphson.component";
import { RegularFalsiComponent } from "./regularfalsi/regularfalsi.component";
import { HomeComponent } from "./home/home.component";
import { SolveComponent } from "./solve/solve.component";
import { CommonComponent } from "./common/common.component";
import { NewtonsForwardInterpolationComponent } from "./newtonsforwardinterpolation/newtonsforwardinterpolation.component";
import { NewtonsBackwardInterpolationComponent } from "./newtonsbackwardinterpolation/newtonsbackwardinterpolation.component";
import { LinearCurveFittingComponent } from "./linearcurvefitting/linearcurvefitting.component";
import { ExponentialCurveFittingComponent } from "./exponentialcurvefitting/exponentialcurvefitting.component";
import { TwoMatrixComponent } from "./2_2/2_2.component";
import { StatisticsComponent } from "./statistics/statistics.component";


export const routes: Routes = [
    { path: '3*3matrix', component: ThreeMatrixComponent },
    { path: 'newton-raphson', component: NewtonRaphsonComponent},
    { path: 'regular-falsi', component: RegularFalsiComponent},
    { path: 'newtons-forward', component: NewtonsForwardInterpolationComponent},
    { path: 'newtons-backward', component: NewtonsBackwardInterpolationComponent},
    { path: 'linear-curvefitting', component: LinearCurveFittingComponent},
    { path: 'exponential-curvefitting', component: ExponentialCurveFittingComponent},
    { path: '2*2matrix', component: TwoMatrixComponent},
    { path: 'statistics', component: StatisticsComponent},
    { path: 'solve', component: SolveComponent},
    { path: 'home', component: HomeComponent},
    { path: '', component: HomeComponent ,pathMatch:"full" }
  ]; 
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        CommonModule,FormsModule,RouterModule.forRoot(routes)
    ],
    declarations: [
        AppComponent,ThreeMatrixComponent,NewtonRaphsonComponent,RegularFalsiComponent,HomeComponent,SolveComponent,CommonComponent,NewtonsForwardInterpolationComponent,NewtonsBackwardInterpolationComponent,LinearCurveFittingComponent,ExponentialCurveFittingComponent,TwoMatrixComponent,StatisticsComponent
    ],
     providers: [
      CommentService
  ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
