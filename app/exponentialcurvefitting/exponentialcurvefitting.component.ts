import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './exponentialcurvefitting.component.html'
})
@Injectable()
export class ExponentialCurveFittingComponent
{
    input: string;
    input3: number;
    input2: number;
    input1: number;
theHtmlString: string;
constructor(private http: Http){ }

send()
{
    this.input=this.input1+' '+this.input2+' '+this.input3;
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
        "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
        "script": "#include<stdio.h>\n#include<math.h>\nint main()\n{int n,i;float Y[20],sumx=0,sumy=0,sumxy=0,sumx2=0,x[20],y[20],sum;float a,b,A;printf(\"\\n  C program for Exponential Curve fitting\\n\");scanf(\"%d\",&n);for(i=0;i<=n-1;i++){scanf(\"%f\",&x[i]);}for(i=0;i<=n-1;i++){scanf(\"%f\",&y[i]);sum=sum+y[i];}for(i=0;i<=n-1;i++){Y[i]=log(y[i]);}printf(\"\\n\\n\\tx\\t\\ty\\t\\tx^2\\t\\tlogy\\t\\tx*logy\");for(i=0;i<=n-1;i++){sumx=sumx +x[i];printf(\"\\n\\n\\t%.2f\",x[i]);printf(\"\\t\\t%.2f\",y[i]);sumx2=sumx2 +x[i]*x[i];printf(\"\\t\\t%.2f\",x[i]*x[i]);sumy=sumy +Y[i];printf(\"\\t\\t%.2f\",log(y[i]));sumxy=sumxy +(x[i]*log(y[i]));printf(\"\\t\\t%.2f\",x[i]*log(y[i]));}printf(\"\\n\\t________________________________________________________________________\");printf(\"\\nsum\\t %.2f\\t\\t%.2f\\t\\t%.2f\\t\\t%.2f\\t\\t%.2f\",sumx,sum,sumx2,sumy,sumxy);A=((sumx2*sumy -sumx*sumxy)*1.0/(n*sumx2-sumx*sumx)*1.0);printf(\"\\n\\nA=(%.3f*%.3f-%.3f*%.3f)*1.0/(%d*%.3f*%.3f*%.3f)*1.0\",sumx2,sumy,sumx,sumxy,n,sumx2,sumx,sumx);printf(\"\\n\\nA=%.3f\",A);b=((n*sumxy-sumx*sumy)*1.0/(n*sumx2-sumx*sumx)*1.0);printf(\"\\n\\nb=(%d*%.3f-%.3f*%.3f)*1.0/(%d*%.3f*%.3f*%.3f)*1.0\",n,sumxy,sumx,sumy,n,sumx2,sumx,sumx);printf(\"\\n\\nb=%.3f\",b);a=exp(A);printf(\"\\n\\nexpA=%.3f\",a);printf(\"\\n\\nY=a*e^bx\");printf(\"\\n\\nThe curve is Y= %4.3fe^%4.3fX\",a,b);return(0);}",
        "language": "c",
        "versionIndex": "0",
        "stdin": this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
       
        },
      (err) => console.log(err));
}
}
