import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './2_2.component.html'
})
@Injectable()
export class TwoMatrixComponent {
    input: any;
    fourth: any;
    third: any;
    second: any;
    first: any;
    main: any;
    splitinputstring: string;
    theHtmlString:any;
    a:any;
    b:any;
    c:any;
    inputstring:string="3\n5\n2\n1\n5\n8\n3\n9\n2";
    determinant:any;

    constructor(private http: Http) {
    }

    send() {
        this.b=this.input.replace(/\s/g,'');

        let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = {
            "clientId": "3fbd417011ba57e19454fc4ce41bcf7e",
            "clientSecret": "28859cb0740300a7d30efd96205c2c3fbc976ec27a78a0dc7a6d0cc98dea22c",
            "script": "#include<stdio.h>\nint main(){int a[2][2]que[5],i,j;int determinant;printf(\"Enter the 4 elements of matrix: \");for(i=0;i<2;i++)for(j=0;j<2;j++)scanf(\"%d\",&a[i][j]);for(i=0;i<2;i++){printf(\"\\n\");for(j=0;j<2;j++)printf(\"%d\\t\",a[i][j]);}printf(\"\\n\\nDeterminant=%d*%d-%d*%d\",a[0][0],a[1][1],a[1][0],a[0][1]);determinant = a[0][0]*a[1][1] - a[1][0]*a[0][1];printf(\"\\n\\nDeterminant of 2X2 matrix: %d\",determinant);printf(\"\\n\\nInverse of matrix=1/determinant* -%d\\t %d\\n\\t\\t\\t \\t%d\\t-%d\",a[0][0],a[1][0],a[0][1],a[1][1]);printf(\"\\n\\nInverse= %d\\t %d\\n\\t %d\\t%d\",-a[0][0]/determinant,a[1][0]/determinant,a[0][1]/determinant,-a[1][1]/determinant);return 0;}",
            "language": "c",
            "versionIndex": "0",
            "stdin":this.input
            
            //"stdin": "3\n3\n3\n5\n2\n1\n5\n8\n3\n9\n2"
        }
       
        this.http.post(Url, body, options).map(res => res.json())
        .subscribe(
          (data) =>{this.theHtmlString= data.output;
            console.log("html"+this.theHtmlString);
             this.main = this.theHtmlString.split('$');
             this.first=this.main[0];
             this.second=this.main[1];
             this.third=this.main[2];
           // this.b = (this.b).replace(/\n/g,'').replace(/\t/g,'');
            },
          (err) => console.log(err));
    }
}