import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './newtonraphson.component.html'
})
@Injectable()
export class NewtonRaphsonComponent
{
    b: any;
    input: any;
    main: any;
    fourth: any;
    third: any;
    second: any;
    first: any;
    program: any;
    nrinput: any;
    theHtmlString: any;
    fx:string;
    fdx:string;
    
    constructor(private http: Http){ }

send()
{
    this.input= this.nrinput
    this.program = "#include<stdio.h>\n#include<math.h>\n#define f(x) "+this.fx+"\n#define fd(x) "+this.fdx+"\n#define E 0.0005\nvoid main(){ float x0,x1,f0,fd0,e;int iter=0;printf(\"\\t\\t\\t\\t\\t\\t\\t--------------------NEWTON-RAPHSON METHOD----------------------\");printf(\"\\n\\nFINDING ROOT OF X^2-4X-10\");printf(\"\\nInitial value:\");scanf(\"%f\",&x0);printf(\"%.2f\",x0);printf(\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tf(x0)\\t\\tfd(x0)\\t\\tx1\\t\\tError\");printf(\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t-----------------------------------------------------\");printf(\"\\nFinding the root by formula x1=x0-(f0/fd0):\");begin:iter++;printf(\"\\n iteration no: %d\",iter);f0=f(x0);printf(\"\\nf0=%.2f*%.2f-4*%.2f-10\",x0,x0,x0);printf(\"\\nf0=%.2f\",f0);fd0=fd(x0);printf(\"\\nf1=2*%.2f-4\",x0);printf(\"\\nfd0=%.2f\",fd0);x1=x0-(f0/fd0);e=fabs((x1-x0)/x1);if(e<E){printf(\"\\n\\nApproximate Root=%.5f\",x1);}else{printf(\"\\n%.2f=%.2f-(%.2f/%.2f)\",x1,x0,f0,fd0);printf(\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t%.2f\\t\\t%.3f\\t\\t%.3f\\t\\t%.4f\",f(x0),fd(x0),x1,e);x0=x1;goto begin;}}";
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
        "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
        "script": this.program,
        "language": "c",
        "versionIndex": "0",
        "stdin": this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
        
        },
      (err) => console.log(err));
}
}