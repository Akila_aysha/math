import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './linearcurvefitting.component.html'
})
@Injectable()
export class LinearCurveFittingComponent
{
    input: string;
    input3: number;
    input2: number;
    input1: number;
theHtmlString: string;
constructor(private http: Http){ }

send()
{
    this.input=this.input1+' '+this.input2+' '+this.input3;
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "3fbd417011ba57e19454fc4ce41bcf7e",
        "clientSecret": "28859cb0740300a7d30efd96205c2c3fbc976ec27a78a0dc7a6d0cc98dea22c",
        "script": "#include<stdio.h>\n#include<math.h>\nint main()\n{int n,i,x[20],y[20],sumx=0,sumy=0,sumxy=0,sumx2=0;float a,b;scanf(\"%d\",&n);for(i=0;i<=n-1;i++){scanf(\" %d\",&x[i]);printf(\"\\n\\nvalues of x%d=%d\",i,x[i]);}for(i=0;i<=n-1;i++){scanf(\"%d\",&y[i]);printf(\"\\n\\nvalues of y%d=%d\",i,y[i]);}printf(\"\\n\\n\\tx\\t\\tx^2\\t\\ty\\t\\tx*y\");for(i=0;i<=n-1;i++){sumx=sumx +x[i];printf(\"\\n\\n\\t%d\",sumx);sumx2=sumx2 +x[i]*x[i];printf(\"\\t\\t%d\",sumx2);sumy=sumy +y[i];printf(\"\\t\\t%d\",sumy);sumxy=sumxy +x[i]*y[i];printf(\"\\t\\t%d\",sumxy);}printf(\"\\n_________________________________________________________________________\");printf(\"\\nsum\\t %d\\t\\t%d\\t\\t%d\\t\\t%d\",sumx,sumx2,sumy,sumxy);a=((sumx2*sumy -sumx*sumxy)*1.0/(n*sumx2-sumx*sumx)*1.0);printf(\"\\n\\na=(%d*%d-%d*%d)*1.0/(%d*%d-%d*%d)*1.0\",sumx2,sumy,sumx,sumxy,n,sumx2,sumx,sumx);printf(\"\\n\\na=%f\",a);b=((n*sumxy-sumx*sumy)*1.0/(n*sumx2-sumx*sumx)*1.0);printf(\"\\n\\nb=(%d*%d-%d*%d)*1.0/(%d*%d-%d*%d)*1.0\",n,sumxy,sumx,sumy,n,sumx2,sumx,sumx);printf(\"\\n\\nb=%f\",b);printf(\"\\n\\ny=a+bx\");printf(\"\\n\\nThe line is Y=%3.3f +%3.3f X\",a,b);return(0);}",
        "language": "c",
        "versionIndex": "0",
        "stdin":this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
       
        },
      (err) => console.log(err));
}
}
