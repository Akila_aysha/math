import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './3_3.component.html'
})
@Injectable()
export class ThreeMatrixComponent {
    input: any;
    fourth: any;
    third: any;
    second: any;
    first: any;
    main: any;
    splitinputstring: string;
    theHtmlString:any;
    a:any;
    b:any;
    c:any;
    inputstring:string="3\n5\n2\n1\n5\n8\n3\n9\n2";
    determinant:any;

    constructor(private http: Http) {
    }

    send() {
        this.b=this.input.replace(/\s/g,'');

        let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = {
            "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
            "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
            "script": "#include<stdio.h>\n#include<math.h>\nvoid trans(float [][40], float [][40], int);\n void cofac(float [][40], int);float determin(float [][40], int);\n int main(){float matrix[40][40], deter;int a, b, row,flag3=0, col;scanf(\"%d %d\", &row, &col); \n printf(\"%d\",row);printf(\"*\");printf(\"%d\\n\",col);if(row==col){printf(\"\\nEnter the elements of %d X %d matrix :\\n\\n\", row, col);for(a=0;a<row;++a){for(b=0;b<col;++b){matrix[a][b]=-1;}}for(a=0;a<row;++a){for(b=0;b<col;++b){scanf(\"%f\",&matrix[a][b]);}}for(a=0;a<row;++a){for(b=0;b<col;++b){if(matrix[a][b]==-1){flag3=1;}}}printf(\"flag3=%d\",flag3);if(flag3==1){printf(\"Invalid Input\");}for(a=0;a<row;++a){for(b=0;b<col;++b){printf(\"%f\\t\",matrix[a][b]);}printf(\"\\n\\n\");}deter = determin(matrix, row);printf(\"The Determinant of the Matrix is :$ %f\\n$\", deter);if(deter == 0)printf(\"\\n Inverse of Matrix is not possible for the matrices having 0 Determinant value !!\\n\");else \n cofac(matrix, row);}else \n printf(\"\\n Inverse of Matrix is possible for Square matrix !! Kindly give same number of rows and columns \\n\");return(0);}float determin(float matrix[40][40],int k){float deter=0.0,z=1.0,mt[40][40];int a,b,c,x,y;if(k==1){return(matrix[0][0]);}else{deter=0;for(a=0;a<k;++a){x=0;y=0;for(b=0;b<k;++b){for(c=0;c<k;++c){mt[b][c]=0;if((b!=0)&&(c!=a)){mt[x][y]=matrix[b][c];if(y<(k-2))y++;else{y=0;x++;}}}}deter=deter+z*(matrix[0][a]*determin(mt,k-1));z=-1*z;}}return(deter);} \n void cofac(float comatr[40][40],int f){float matr[40][40],cofact[40][40];int a,b,c,d,x,y;printf(\"\\nThe Co factor of input matrix is...\\n\\n\");for(c=0;c<f;++c){for(d=0;d<f;++d){x=0;y=0;for(a=0;a<f;++a){for(b=0;b<f;++b){if(a!=c&&b!=d){matr[x][y]=comatr[a][b];if(y<(f-2))y++;else{y=0;x++;}}}}cofact[c][d]=pow(-1,c+d)*determin(matr,f-1);printf(\"%.2f\\t\",cofact[c][d]);}printf(\"\\n\\n\");}trans(comatr,cofact,f);} \n void trans(float matr[40][40],float m1[40][40],int r){float inv_matrix[40][40],tran[40][40],d;int a,b;for(a=0;a<r;++a){for(b=0;b<r;++b){tran[a][b]=m1[b][a];}}d=determin(matr,r);for(a=0;a<r;++a){for(b=0;b<r;++b){inv_matrix[a][b]=tran[a][b]/d;}}printf(\"$\\nThe Inverse of matrix is...\\n\\n\");for(a=0;a<r;++a){for(b=0;b<r;++b)printf(\"%f\\t\",inv_matrix[a][b]);printf(\"\\n\\n\");}}",
            "language": "c",
            "versionIndex": "0",
            "stdin":"3 3 "+this.input
            
            //"stdin": "3\n3\n3\n5\n2\n1\n5\n8\n3\n9\n2"
        }
       
        this.http.post(Url, body, options).map(res => res.json())
        .subscribe(
          (data) =>{this.theHtmlString= data.output;
            console.log("html"+this.theHtmlString);
             this.main = this.theHtmlString.split('$');
             this.first=this.main[0];
             this.second=this.main[1];
             this.third=this.main[2];
             this.fourth=this.main[3];
           // this.b = (this.b).replace(/\n/g,'').replace(/\t/g,'');
            },
          (err) => console.log(err));
    }
}