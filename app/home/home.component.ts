import { Component, AfterViewInit } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { RouterLink } from '@angular/router';
@Component({
    
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './home.component.html'
})
@Injectable()
export class HomeComponent implements AfterViewInit{
    ngAfterViewInit () {
        $('.navbar-nav li a').click(function (e) {
            var $this = $(this);
            $this.parent().siblings().removeClass('active').end().addClass('active');
            $(".navbar-toggle").removeClass('active');
            e.preventDefault();
        });
        
        $('.about .panel-heading a').click(function() {
            $('.about .panel-heading').removeClass('actives');
            $(this).parents('.panel-heading').addClass('actives');
            });
      }
}