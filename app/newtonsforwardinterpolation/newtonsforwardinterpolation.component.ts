import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './newtonsforwardinterpolation.component.html'
})
@Injectable()
export class NewtonsForwardInterpolationComponent {
    f: any;
    y: any;
    x: any;
    n: any;
    inpustring: any;
    first: any;
    input4: any;
    input3: any;
    input2: any;
    input1: any;
    input: any
    theHtmlString: any;
    constructor(private http: Http) { }

    send() {
        this.input=this.input1+' '+this.input2+' '+this.input3+' '+this.input4;
        //this.input = "4 0 1 2 3 1 3 7 13 0.1";
        let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = {
            "clientId": "3fbd417011ba57e19454fc4ce41bcf7e",
            "clientSecret": "28859cb0740300a7d30efd96205c2c3fbc976ec27a78a0dc7a6d0cc98dea22c",
            "script": "#include<stdio.h>\n#include<math.h>\n#include<stdlib.h>\nint main(){float x[20],y[20],f,s,h,d,p;int j,i,n;printf(\"enter the value of n :$\");scanf(\"%d\",&n);printf(\"enter the elements of x:$\");for(i=1;i<=n;i++){scanf(\"%f\",&x[i]);}printf(\"enter the elements of y:$\");for(i=1;i<=n;i++){scanf(\"%f\",&y[i]);}printf(\"Enter the value of f:$\");scanf(\"%f\",&f);h=x[2]-x[1];printf(\"h=x[2]-x[1]\\n\\n\");printf(\"h=x[2]-x[1]\\n\\n h=%f-%f\\n h=%f\\n\\n\",x[2],x[1],h);s=(f-x[1])/h;printf(\"p=(x-x0)/h \\np=(%f-%f)/%f \\np=%f\\n\\n\",f,x[1],h,s);p=1;d=y[1];printf(\"Difference between corresponding y values\\n\\n\");for(i=1;i<=(n-1);i++){for(j=1;j<=(n-i);j++){y[j]=y[j+1]-y[j];printf(\"%f\\t\",y[j]);}printf(\"\\n\\n\");p=p*(s-i+1)/i;d=d+p*y[1];}printf(\"To find the value of f(x), we use the formula:\\n\\n f(x)=y0+p(dy0)+(p(p-1)(d^2*y0))/2!+.....\\n\\n\");printf(\"%.4f=%.4f*(%.4f-%d+1)/%d\\n\\n\",p,p,s,i,i);printf(\"%.4f=%.4f+%.4f*%.4f\\n\\n\",d,d,p,y[1]);printf(\"\\nFor the value of x=%6.5f THe value is %6.5f\",f,d);}",
            "language": "c",
            "versionIndex": "0",
            "stdin": this.input
        }
        this.http.post(Url, body, options).map(res => res.json())
            .subscribe(
                (data) => {
                this.theHtmlString = data.output;
                    console.log("html" + this.theHtmlString);
                    this.inpustring = this.theHtmlString.split('$');
                    this.n = this.inpustring[0];
                    this.x = this.inpustring[1];
                    this.y = this.inpustring[2];
                    this.f = this.inpustring[3];
                    this.first = this.inpustring[4];
                },
                (err) => console.log(err));
    }
}
