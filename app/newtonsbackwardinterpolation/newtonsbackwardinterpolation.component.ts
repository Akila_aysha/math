import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './newtonsbackwardinterpolation.component.html'
})
@Injectable()
export class NewtonsBackwardInterpolationComponent
{
    input: any;
    input4: any;
    input3: any;
    input2: any;
    input1: any;
theHtmlString: string;
constructor(private http: Http){ }

send()
{
    this.input= this.input1+' '+this.input2+' '+this.input3+' '+this.input4;
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
        "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
        "script": "#include<stdio.h>\n#include<math.h>\nint main()\n{float x[10],y[10][10],sum,p,u,temp;int i,n,j,k=0,f,m;float fact(int);printf(\"\\nhow many record you will be enter: \");scanf(\"%d\",&n);for(i=0; i<n; i++){scanf(\"%f\",&x[i]);}for(i=0; i<n; i++){scanf(\"%f\",&y[k][i]);}printf(\"\\n\\nEnter X for finding f(x): \");scanf(\"%f\",&p);for(i=1;i<n;i++){for(j=i;j<n;j++){y[i][j]=y[i-1][j]-y[i-1][j-1];printf(\"\\n\\n%.3f=%.3f-%.3f\",y[i][j],y[i-1][j],y[i-1][j-1]);}}printf(\"\\n_____________________________________________________\\n\");printf(\"\\n  x(i)\\t   y(i)\\t    y1(i)    y2(i)    y3(i)    y4(i)\");printf(\"\\n_____________________________________________________\\n\");for(i=0;i<n;i++){printf(\"\\n %.3f\",x[i]);for(j=0;j<=i;j++){printf(\"   \");printf(\" %.3f\",y[j][i]);}printf(\"\\n\");}i=0;do{if(x[i]<p && p<x[i+1])k=1;else\ni++;}while(k != 1);f=i+1;u=(p-x[f])/(x[f]-x[f-1]);printf(\"\\n\\nu=(%.3f-%.3f)/(%.3f-%.3f)\",p,x[f],x[f],x[f-1]);printf(\"\\n\\n u = %.3f \",u);n=n-i+1;sum=0;for(i=0;i<n;i++){temp=1;for(j=0;j<i;j++){temp = temp * (u + j);}m=fact(i);sum = sum + temp*(y[i][f]);printf(\"\\n\\nsum=%.3f+%.3f*(%.3f/%d)\",sum,temp,y[i][f],m);}printf(\"\\n\\n f(%.2f) = %f \",p,sum);}\nfloat fact(int a)\n{float fac = 1;if (a == 0)return (1);else\nfac = a * fact(a-1);return(fac);}",
        "language": "c",
        "versionIndex": "0",
        "stdin": this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
       
        },
      (err) => console.log(err));
}
}
