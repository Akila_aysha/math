import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './statistics.component.html'
})
@Injectable()
export class StatisticsComponent
{input: any;
input1: any;
input2: any;
program: any;
nrinput: any;
theHtmlString: string;
constructor(private http: Http){ }

send()
{
    this.input=+this.input1+' '+this.input2;
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "3fbd417011ba57e19454fc4ce41bcf7e",
        "clientSecret": "28859cb0740300a7d30efd96205c2c3fbc976ec27a78a0dc7a6d0cc98dea22c",
        "script":"#include<stdio.h>\nint main()\n{int i,j,a[20]={0},sum=0,n,t,b[20]={0},k=0,c=1,max=0,mode,z;float x=0.0,y=0.0;scanf(\"%d\",&n);for(i=0;i<n;i++){scanf(\"%d\",&a[i]);sum=sum+a[i];}printf(\"To calculate the mean\");printf(\"\\n\\nSum=%d\",sum);printf(\"\\n\\nMean=total sum/no of elements\");x=(float)sum/(float)n;printf(\"\\n\\nMean=%d/%d\",sum,n);printf(\"\\n\\nMean\\t= %f\",x);for(i=0;i<n;i++){for(j=i+1;j<n;j++){if(a[i]>a[j]){t=a[i];a[i]=a[j];a[j]=t;}}}printf(\"\\n\\nTo calculate median first sort the elements in ascending order\\n\\n\");for(j=0;j<n;j++)printf(\"%d\\t\",a[j]);printf(\"\\n\\nIf there is an odd number of results, the median is the middle number.\\nif there is an even number of results, the median will be the mean of the two central numbers\");if(n%2==0){printf(\"\\n\\nIf the numvers are even in count\");y=(float)(a[n/2]+a[(n-1)/2])/2;printf(\"\\n\\nMedian=(((n/2)+(n+2/2))/2)th element\");printf(\"\\n\\n(%d+%d/2)th\",(n/2),(n+2)/2);printf(\"\\n\\nMedian=(%d+%d)/2\",a[n/2],a[(n-1)/2]);}else\nprintf(\"\\n\\nIf there are odd number of elements\");printf(\"\\n\\n(n+1/2)th element\");y=a[(n-1)/2];printf(\"\\n\\n%d+1/2 element\",n);printf(\"\\n\\nMedian\\t= %f\",y);printf(\"\\n\\nMode is the most repeated element\");for(i=0;i<n-1;i++){mode=0;for(j=i+1;j<n;j++){if(a[i]==a[j]){mode++;}}if((mode>max)&&(mode!=0)){k=0;max=mode;b[k]=a[i];k++;}else\nif(mode==max){b[k]=a[i];k++;}}for(i=0;i<n;i++){if(a[i]==b[i])c++;}if(c==n)printf(\"\\n\\nThere is no mode\");else{printf(\"\\nMode\\t= \");for(i=0;i<k;i++)printf(\"%d \",b[i]);return 0;}}",
        "language": "c",
        "versionIndex": "0",
        "stdin":this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
       
        },
      (err) => console.log(err));
}
}
