import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './regularfalsi.component.html'
})
@Injectable()
export class RegularFalsiComponent
{ first: string;
n: string;
s: string;
inpustring: string[];
input2: any;
input1: any;
input: any;
fm: any;
fx: any;
fdx: string;
program: any;
nrinput: any;
theHtmlString: string;
constructor(private http: Http){ }

send()
{
    this.input=this.input1+' '+this.input2+' 0.0005';
    this.program = "#include<stdio.h>\n#include<stdlib.h>\n#include<math.h>\n#define function(m) "+this.fm+"\nint main()\n{float start_point,end_point,error=0,midpoint,temp,allowed_error;int count=0;printf(\"Enter Interval Start Point:$\");scanf(\"%f\",&start_point);printf(\"Enter Interval End Point:$\");scanf(\"%f\",&end_point);printf(\"\\n\");if((function(start_point)*function(end_point))>0){printf(\"Invalid Intervals \\n\");exit(0);}else\nif(function(start_point)==0||function(end_point)==0){printf(\"Root:\\t%f\\n\",function(start_point)==0?start_point:end_point);exit(0);}printf(\"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\tID\\tStart\\t\\tEnd\\t  Mid-Point\\tf(Mid-Point)\\t\\n\");printf(\"\\nmidpoint=((startpoint*f(endpoint))-(endpoint*f(startpoint))/(f(endpoint)-f(startpoint))\\n\");do\n{temp=midpoint;midpoint=((start_point*function(end_point))-(end_point*function(start_point)))/(function(end_point)-function(start_point));printf(\"\\nmidpoint=((%.3f*%.3f)-(%.3f*%.3f))/(%.3f-%.3f)\",start_point,function(end_point),end_point,function(start_point),function(end_point),function(start_point));printf(\"\\nmidpoint=%.3f\",midpoint);printf(\"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t%d\\t%.3f\\t\\t%.3f\\t\\t%.3f\\t\\t%.3f\\t\",count++,start_point,end_point,midpoint,function(midpoint));if(function(midpoint)==0){break;}else\nif(function(start_point)*function(midpoint)<0){printf(\"\\nfunction(start_point)*function(midpoint)<0\");printf(\"\\nend_point=midpoint\");end_point=midpoint;printf(\"\\nend_point=%.3f\",midpoint);printf(\"\\n\");}else{printf(\"\\nfunction(start_point)*function(midpoint)>0\");printf(\"\\nstart_point=midpoint;\");start_point=midpoint;printf(\"\\nstart_point=%.3f\",midpoint);printf(\"\\n\");}error=fabs(midpoint-temp);if(count==1){printf(\"\\n\");}}while(error>0.0005);printf(\"\\n\\nRoot of the Equation:\\t%f\\n\",midpoint);return 0;}";
    let Url = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = {
        "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
        "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
        "script": this.program,
        "language": "c",
        "versionIndex": "0",
        "stdin": this.input
    }
    this.http.post(Url,body,options).map(res => res.json())
    .subscribe(
      (data) =>{this.theHtmlString= data.output;
        console.log("html"+this.theHtmlString);
        this.inpustring = this.theHtmlString.split('$');
                    this.s = this.inpustring[0];
                    this.n = this.inpustring[1];
                    this.first = this.inpustring[2];

        
       
        },
      (err) => console.log(err));
}
}
