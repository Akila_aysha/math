
$(document).ready(function () {
//   setTimeout(function(){ 
AOS.init();

$(window).scroll(function () {
    if ($(window).scrollTop() >= 120) {
        $('header .navbar').addClass('fixed-top bg-dark');
    }
    else {
        $('header .navbar').removeClass('fixed-top bg-dark');
    }
});


/* Bind Scroll */
$('.page-scroll').bind('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 15)
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
});


    /* Add active to menu */
$('.navbar-nav li a').click(function (e) {
    var $this = $(this);
    $this.parent().siblings().removeClass('active').end().addClass('active');
    $(".navbar-toggle").removeClass('active');
    e.preventDefault();
});

$('.about .panel-heading a').click(function() {
    $('.about .panel-heading').removeClass('actives');
    $(this).parents('.panel-heading').addClass('actives');
    });



// quick search regex
var qsRegex;

// init Isotope
var $grid = $('.grid').isotope({
itemSelector: '.element-item',
layoutMode: 'fitRows',
filter: function() {
return qsRegex ? $(this).text().match( qsRegex ) : true;
}
});

// use value of search field to filter
var $quicksearch = $('.quicksearch').keyup( debounce( function() {
qsRegex = new RegExp( $quicksearch.val(), 'gi' );
$grid.isotope();
}, 200 ) );

// debounce so filtering doesn't happen every millisecond
function debounce( fn, threshold ) {
var timeout;
return function debounced() {
if ( timeout ) {
clearTimeout( timeout );
}
function delayed() {
fn();
timeout = null;
}
timeout = setTimeout( delayed, threshold || 100 );
}
}

// }, 500);
});